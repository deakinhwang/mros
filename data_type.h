#ifndef DATA_TYPE_H
#define DATA_TYPE_H


#include <vector>
#include <Eigen/Core>
#include <Eigen/Geometry>

#include "color_type.h"

///mros Common DataType
namespace mros {

//--------------------base info-------------------------
////机器人位态以及运动速度
typedef struct PoseT{
    Eigen::Vector3d position;
    Eigen::Quaterniond orientation;
    Eigen::Vector3d velocity;
}PoseT;

//--------------------visualization_marker_publisher-------------------------
////marker可选颜色
typedef enum VisualizationMarkerColorsL{
  VISUALIZATION_MARKER_COLOR_WHITE = 0x00,
  VISUALIZATION_MARKER_COLOR_BLACK,
  VISUALIZATION_MARKER_COLOR_RED,
  VISUALIZATION_MARKER_COLOR_ORANGE,
  VISUALIZATION_MARKER_COLOR_YELLOW,
  VISUALIZATION_MARKER_COLOR_GREEN,
  VISUALIZATION_MARKER_COLOR_BLUE,
  VISUALIZATION_MARKER_COLOR_CYAN,
  VISUALIZATION_MARKER_COLOR_VIOLET,
}VisualizationMarkerColorsL;

///marker大小
typedef enum VisualizationMarkerScaleL{
  VISUALIZATION_MARKER_SCALE_01 = 0x00,
  VISUALIZATION_MARKER_SCALE_02,
  VISUALIZATION_MARKER_SCALE_03,
  VISUALIZATION_MARKER_SCALE_04,
  VISUALIZATION_MARKER_SCALE_05,
  VISUALIZATION_MARKER_SCALE_06,
}VisualizationMarkerScaleL;

//marker类型
typedef enum VisualizationMarkerTypesL{
  VISUALIZATION_MARKER_TYPE_POINTS = 0x00,
  VISUALIZATION_MARKER_TYPE_CUBE_LIST,
  VISUALIZATION_MARKER_TYPE_LINE_LIST,
  VISUALIZATION_MARKER_TYPE_LINE_STRIP
}VisualizationMarkerTypesL;


typedef struct VisualizationMarkerPubParametersT{
  std::string topic;        //要发布到的主题
  int auto_publish_fq ;     //定时器自动发送频率，小于零则不开定时器
  std::string frame_id;     //参考ID
  int points_max;           ///点数最大值,-1则不做限制
  //int base_id;              //当前marker参考id, 实际发布id不能重复，否则会覆盖
  //VisualizationMarkerTypesL type;
  //VisualizationMarkerColorsL color;
  //VisualizationMarkerScaleL scale;
}VisualizationMarkerPubParametersT;

static VisualizationMarkerPubParametersT VisualizationMarkerPubParameters(
    const std::string& topic,
    const int& auto_publish_fq,
    const std::string& frame_id,
    const int points_max
    //const int base_id,
    //const VisualizationMarkerTypesL& type,
    //const VisualizationMarkerColorsL& color,
    //const VisualizationMarkerScaleL& scale,
    ){

  VisualizationMarkerPubParametersT params;
  params.topic = topic;
  params.auto_publish_fq = auto_publish_fq;
  params.frame_id = frame_id;
  params.points_max = points_max;
  //params.base_id = base_id;
  //params.type = type;
  //params.color = color;
  //params.scale = scale;
  return  params;
}

typedef struct VisualizationMarkerParametersT{
  int base_id;
  VisualizationMarkerTypesL type;
  VisualizationMarkerColorsL color;
  VisualizationMarkerScaleL scale;
  //int points_max;
}VisualizationMarkerParametersT;

static VisualizationMarkerParametersT VisualizationMarkerParameters(
    const int base_id,
    const VisualizationMarkerTypesL& type,
    const VisualizationMarkerColorsL& color,
    const VisualizationMarkerScaleL& scale
    //const int points_max
    ){
  VisualizationMarkerParametersT params;
  params.base_id = base_id;
  params.type = type;
  params.color = color;
  params.scale = scale;
  //params.points_max = points_max;
  return  params;
}

//-------------------------------------------------------------------------------



}  //end of namespace mros

#endif // DATA_TYPE_HPP


