#ifndef PARAMETER_LOAD_HPP
#define PARAMETER_LOAD_HPP

#include <iostream>

#include <ros/ros.h>

#include <Eigen/Core>
#include <Eigen/Geometry>
#include "parameter.hpp"

namespace mros {


static Eigen::Vector3d LoadParamVector3d( const std::shared_ptr<mros::Parameter> parameter, const std::string& key ){
  Eigen::Vector3d out;
  std::vector<double> vec;
  parameter->Get<std::vector<double>>( key, vec );
  out.x() = vec[0];
  out.y() = vec[1];
  out.z() = vec[2];
  return out;
}

static Eigen::Vector2d LoadParamVector2d( const std::shared_ptr<mros::Parameter> parameter, const std::string& key ){
  Eigen::Vector2d out;
  std::vector<double> vec;
  parameter->Get<std::vector<double>>( key, vec );
  out.x() = vec[0];
  out.y() = vec[1];
  return out;
}

/// 加载8点区域数据
static void LoadPoints8Area( std::vector<Eigen::Vector2d>& area,
               const std::shared_ptr<mros::Parameter> parameter,
               const std::string& pre_key ){
  area.push_back( LoadParamVector2d( parameter, pre_key + "_p00" ) );
  area.push_back( LoadParamVector2d( parameter, pre_key + "_p01" ) );
  area.push_back( LoadParamVector2d( parameter, pre_key + "_p02" ) );
  area.push_back( LoadParamVector2d( parameter, pre_key + "_p03" ) );
  area.push_back( LoadParamVector2d( parameter, pre_key + "_p04" ) );
  area.push_back( LoadParamVector2d( parameter, pre_key + "_p05" ) );
  area.push_back( LoadParamVector2d( parameter, pre_key + "_p06" ) );
  area.push_back( LoadParamVector2d( parameter, pre_key + "_p07" ) );
}


/// 加载4点区域数据
static void LoadPoints4Area( std::vector<Eigen::Vector2d>& area,
               const std::shared_ptr<mros::Parameter> parameter,
               const std::string& pre_key ){
  area.push_back( LoadParamVector2d( parameter, pre_key + "_p00" ) );
  area.push_back( LoadParamVector2d( parameter, pre_key + "_p01" ) );
  area.push_back( LoadParamVector2d( parameter, pre_key + "_p02" ) );
  area.push_back( LoadParamVector2d( parameter, pre_key + "_p03" ) );
}


} // end of namespace mros

#endif // PARAMETER_LOAD_HPP

