#ifndef PARAMETER_HPP
#define PARAMETER_HPP

#include <ros/ros.h>

namespace mros {

class Parameter{

public:
  explicit Parameter( const ros::NodeHandlePtr nh ) : nh_( nh ) {
    ;
  }

  template<typename T>
  void Get(const std::string& param_name , T& param )  {
    bool ret =  nh_->getParam( param_name, param );
    if( !ret ){
      ROS_ERROR("get param [ %s ] fail!", param_name.c_str() );
      exit(1);
    }
  }

  template<typename T>
  void Get(const std::string& param_name , T& param, const T& param_default_val )  {
    param = nh_->param<T>( param_name, param_default_val );
  }

  template<typename T>
  void Set( const std::string& param_name , const T& set_val  )  {
    nh_->setParam( param_name, set_val );
  }

  ~Parameter(){
    ;
  }

private:
  ros::NodeHandlePtr nh_;

};


}

#endif // PARAM_HPP
